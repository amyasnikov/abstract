
# Формат заголовка IPv4

### Заголовок IPv4-пакета

* Приведены актуальные названия полей, ранее некоторые из них назывались иначе
* Никаких других полей в заголовке нет!

```
+-------+-------+-------+-------+-------------------------------+
|Version|  IHL  | DSCP  |  ECN  |         Total Length          |
+-------+-------+-------+-------+-------+-----------------------+
|        Identification         | Flags |    Fragment Offset    |
+---------------+---------------+-------+-----------------------+
| Time to live  |   Protocol    |        Header Checksum        |
+---------------+---------------+-------------------------------+
|                        Source Address                         |
+---------------------------------------------------------------+
|                     Destination Address                       |
+---------------------------------------------------------------+
|                            Options                            |
+---------------------------------------------------------------+
```


### Поле Total Length

* Размер поля: 16 бит
* Указывает полный размер пакета (фрагмента), включая заголовок и данные
  * Зная размер заголовка из поля IHL можно узнать размер вложенных данных, полезно при вложении маленьких пакетов в Ethernet
  * Минимальный размер - 20 байт (пустой пакет)
  * Максимальный размер - 65535 байт
  * Узел обязан поддерживать размер до 576



### Identification, Flags, Fragment Offset

* Поля для управления фрагментацией
* Как правило, канальные среды не пропускают слишком большие кадры
  * Максимальный размер вложения в стандартный Ethernet - 1500 байт
* Если размер отправляемого через интерфейс IP-пакета превышает MTU этого интерфейса, есть два возможных действия:
  * Не отправлять пакет вовсе
  * Разрезать данные из пакета на части подходящего размера и отправлять фрагменты в отдельных пакетах



### Поле Identification

* Размер: 16 бит
* Предоставляется отправителем у всех пакетов, у каждого следующего пакета по сравнению с предыдущим ID возрастает на единицу
* При фрагментации пакета ID фрагментов наследуется от исходного пакета
  * Получатель легко сможет отделить фрагменты разных пакетов друг от друга
* Исторически поле ID использовалось и для определения задублированности пакетов, но сегодня RFC 6864 запрещает это поведение



### Поле Fragment Offset

* Размер: 13 бит
* Указывает смещение данных фрагмента относительно начала оригинального пакета
  * У оригинального пакета там 0
* Задается в 8-байтовых блоках (т.е. для получения номера байта нужно значение Fragment Offset умножить на 8)



### Поле Flags

* Три битовых флага:
  * Reserved, всегда равно 0
  * Don't Fragment (DF), выставляется в 1 для указания запрета на фрагментацию
  * More Fragments (MF), выставляется в 1 для всех фрагментов, кроме последнего



### Поле Time to Live

* Размер: 8 бит
* Изначальное назначение: указание "срока годности" пакета в секундах, поскольку маршрутизация была медленной
  * Каждый маршрутизатор, обслуживающий пакет, вычитал из значения поля время в секундах, которое заняла маршрутизация, но не менее 1
  * Если получается 0 или меньше, пакет сбрасывается
* Сегодня используется для примитивной защиты от перегрузки маршрутизаторов при возникновении петель маршрутизации
  * Начальное значение отправитель выставляет по своему усмотрению, обычно 64 или 128



### Поле Protocol

* Размер: 8 бит
* Указывает формат вложенных данных
  * Обычно это протокол транспортного уровня
  * Значения присваивает IANA
* Популярные значения:
  * 1: ICMP (Internet Control Message Protocol)
  * 6: TCP (Transmission Control Protocol)
  * 17: UDP (User Datagram Protocol)
  * 47: GRE (Generic Routing Encapsulation)
  * 50: ESP (Encapsulation Security Payload)



### Поле Header Checksum

* Размер: 16 бит
* Проверяет корректность заголовка IPv4
  * Не проверяет корректность данных
* Использует алгоритм Internet Checksum
  * Вычислительно несложный
  * Приемлемо обнаруживает ошибки линии
  * Не защищает от умышленных искажений



### Алгоритм Internet Checksum

* Как посчитать Internet Checksum?
  * Проверяемые данные разбиваются на 16-битные слова, которые затем суммируются
  * При результате больше 0xFFFF повторяем шаг 1 до получения 16-битного слова
  * Результат вычитается из 0xFFFF
* Пример: E34F 2396 4427 99F3
  * E34F + 2396 + 4427 + 99F3 = 1E4FF
  * 1 + E4FF = E500
  * FFFF - E500 = 1AFF
* Как проверить правильность чексуммы?
  * Посчитать чексумму от проверяемых данных вместе с чексуммой, должно получиться 0x0000



### Поля Source / Destination Address

* Размер каждого поля - 32 бита
* Указываются IP-адреса исходного отправителя и конечного получателя
* Поля не модифицируются во время марщрутизации (кроме сценариев с NAT)



### Поле Options

* Размер от 0 до 40 байт, кратен 4 байтам
* Создавалось для расширения заголовка IPv4
* Стандартные опции (RFC 791, 1108, 1385, 1393, 2113, 4782, 5350) в реальном мире не используются (экспериментальные, устаревшие или небезопасные)
* Нестандартные опции поддерживаются не всеми производителями оборудования
* Как результат - поле не используется никем, часто пакеты с опциями фильтруются



