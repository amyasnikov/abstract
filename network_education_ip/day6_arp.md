
# ARP

### ARP: Ethernet Address Resolution Protocol

* Альтернативное название RFC: Converting Network Protocol Addresses to 48.bit Ethernet Address for Transmission on Ethernet Hardware
* Может работать с любым протоколом, использующим MAC-адреса и поддерживающим широковещательную расылку



### Принцип работы ARP

* В какой ситуации необходим ARP?
  * "Отправителю" требуется отправить узлу, находящемуся с ним в одной сети Ethernet ("получателю"), IP-пакет
  * IP-адрес получателя известен
  * MAC-адрес получателя неизвестен
* ARP запрашивает MAC получателя, указывая его IP
  * Запрос идет в широковещательном кадре
  * "Получатель" видит свой IP-адрес в запросе и отправляет в ответ кадр со своими IP и MAC-адресом
  * "Отправитель" зная MAC-адрес, отправляет оригинальный пакет



### Кэш ARP

* Отправить запрос ARP на каждый пакет накладно
* IP-адреса и MAC-адреса у узлов меняются редко
* Разумно в течение некоторого времени хранить известные пары IP + MAC в промежуточной таблице
  * Пополнять таблицу автоматически при получнии любого ARP-пакета (он содержит IP и MAC отправителя)
  * Но не хранить привязки вечно, они должны устаревать
* Если запись есть в таблице - пакет отправляется, если нет - откладывается в буфер или удаляется



### Формат пакета ARP

* В случае IP + Ethernet:
  * Инкапсуляция: ARPA, EtherType: 0x0806
  * HTYPE: 0x0001, PTYPE: 0x0800
  * HLEN: 0x06, PLEN: 0x04
* OPER:
  * 0x0001 для запроса
  * 0x0002 для ответа

| Octet offset |                                         |
|--------------|-----------------------------------------|
| 0            | Hardware type (HTYPE)                   |
| 2            | Protocol type (PTYPE)                   |
| 4            | Hardware address length (HLEN)          |
| 5            | Protocol address length (PLEN)          |
| 6            | Operation (OPER)                        |
| 8            | Sender hardware address (SHA) (6 bytes) |
| 14           | Sender protocol address (SPA) (4 bytes) |
| 18           | Target hardware address (THA) (6 bytes) |
| 24           | Target protocol address (TPA) (2 bytes) |



### Proxy ARP

* Если нарушен принцип "одна канальная среда - одна сеть IP", то стандартное поведение ARP не позволит узнать привязку IP к канальному адресу
* Proxy ARP - костыль для подобных случаев
  * MAC получателя на самом деле не интересен
  * Маршрутизатор возвращает свой MAC в ответ
* Может использоваться вместо default gateway
  * Но не стоит: вызывает переполнение кэша ARP



### Конфликты IP-адресов

* ARP может использоваться перед назначением IP-адреса на интерфейс для проверки его незанятости
  * Или для поднятия тревоги, если кто-то другой пытается выдать свой MAC для вашего IP
* ARP Probe (ACD, RFC 5227): OPER=1, SPA=0, TPA=IP
  * Используется перед инициализацией адреса
* Gratuitous ARP (ARP Announcement):
  * Два режима рабты
    * OPER=1, SPA=TPA=IP, THA=0
    * OPER=2, SPA=TPA=IP, SHA=THA
  * Используется после инициализации адреса для обновления кэша остальных клиентов в сети



### Смежные с ARP протоколы

* Протоколы, основанные на ARP:
  * Reverse Adress Resolution Protocol (RARP)
  * Inverse Adress Resolution Protocol (InARP)
* Протоколы, реализующие схожие функции:
  * SLARP (Serial Line Adress Resolution Protocol), подпротокол Cisco HDLC
  * NDP (Neighbor Discovery Protocol), подпротокол ICMPv6



