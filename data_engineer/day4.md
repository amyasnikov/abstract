
# Дистрибутивы Cloudera и Hortonworks

### Предпосылки появления Hadoop

* Потребность в распределенных хранилищах
* Масштабирование вычислений
* Управление ресурсами



### Flume

* Apache Flume - инструмент для транспортировки данных между различными источниками и хранилищами



### Sqoop

* Apache Sqoop - по задачам похож на Flume, но ориентирован на загрузку данных из реляционных баз в Hadoop
* Под капотом использует MR



### Pig

* Apache Pig – это высокоуровневый процедурный язык, предназначенный для выполнения запросов к большим слабо структурированным наборам данных с помощью платформ Hadoop и MapReduce
* Pig упрощает использование Hadoop, позволяя выполнять SQL-подобные запросы к распределенным наборам данных



### Hive

* Позволяет выполнять запросы к слабоструктурированным данным
* Для запросов используется HiveQL
* Имеет свой metastore для хранения "данных о данных" (структур таблиц)



### HBase

* Wide-column database
* По сути, KV база данных поверх HDFS
* Хорошо подходит для разреженных данных
* Основные понятия
  * Table
  * Row
  * Column family
  * Cell



### Oozie

* Oozie - родной оркестратор Hadoop
* Позволяет планировать задачи
* Позволяет строить сложные пайплайны обработки данных
* Интегрирован с MR, Spark, HDFS, Sqoop



### История Hadoop

* 2003 - Google File System paper
* 2004 - MapReduce paper
* 2006 - Hadoop project
* 2008 - Pig, Hive, Zookeeper, HBase
* 2009 - Amazon EMR
* 2008 - создание Cloudera
* 2011 - выделение Hortonworks



### Зачем нужны дистрибутивы Hadoop?

* Интеграция компонентов
* Синхронизация версий
* Мониторинг
* Дополнительные инструменты



### Что за продукт у Cloudera?

* CDH - Cloudera’s Distribution including Apache Hadoop (free & enterprise)
* Cloudera Manager
* Cloudera Data Science Workbench



### Инструменты Cloudera

* Kudu - аналитическое хранилище данных (с ориентацией на time series данные)
* Impala - движок для ad-hoc аналитики
* Sentry - безопасность



### Развертывание "боевого" кластера CDH

* Подготовить сервера и создать базы данных
* Установить Cloudera Manager
  * Добавить репозиторий Cloudera
  * Установить пакет
* Через Cloudera Manager
  * Add cluster
  * Add hosts (указать машины и предоставить ключ пользователя с sudo-доступом)
* Добавить на новые машины роли сервисов



### Что за продукт у Hortonworks?

* Hortonworks Data Platform (HDP)
* Hortonworks DataFlow (HDF)
* Ambari (Cluster management)



### Инструменты Hortonworks

* Knox - proxy для безопасного доступа
* Phoenix - аналитическая база поверх HBase
* Ranger - контроль доступа к Hadoop
* Atlas - data linage (карта данных)
* Ni-Fi - data ingestion tool



