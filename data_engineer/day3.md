
# Облачные платформы. GCP, AWS, Azure

### Главные облачные платформы

* Google Cloud Platform
* Amazon Web Services
* Microsoft Azure



### Главные облачные платформы

* Object storage
  * Cloud Storage (GCP)
  * S3 (AWS)
* Message queue
  * Pub/Sub (GCP)
  * Kinesis / Managed Kafka (AWS)
* Data ingestion tool
  * Cloud Data Fusion (GCP)
  * Glue (AWS)
* Compute engine
  * Dataflow / Dataproc (GCP)
  * EMR (AWS)
* MPP SQL database
  * BigQuery (GCP)
  * Redshift / Athena (AWS)
* Operational database
  * BigTable (KV), Spanner (SQL) (GCP)
  * DynamoDB (KV), Aurora (SQL) (AWS)
  * ... и множество других под конкретные паттерны хранения и доступа
* Orchestration tool
  * Composer (GCP)
  * Data Pipeline (AWS)



### Cloud Storage

* Облачное гео-распределенное хранилище для файлов (не путать с Google Drive)
* Основной элемент - bucket (сегмент в русской версии)



### Cloud Storage - Buckets

* Большая часть настроек задается на уровне этих самых бакетов:
  * Регион
  * Версионирование
  * Класс хранилища
  * Политики хранения
  * Шифрование



### Pub/Sub

* Очередь сообщений
* Возможность репроцессинга
* Глобальная репликация
* Exactly-once delivery
* Auto-scaling
* Основные элементы - topic, subscription
* Нет партиций (как в Kafka)



### Dataproc

* Managed Spark + Hadoop + Hive
* Кластер по запросу
* Интеграция с экосистемой GCP



### Dataflow

* Также известный как Apache Beam
* Использует концепцию универсального подхода к batch и streaming процессингу данных
* Запускается на нескольких бэкендах, включая основной - сервис Google Dataflow



### BigQuery

* SaaS-решение из экосистемы Google Cloud
* Оплата за объем обработанных данных
* Поддерживает слабо-структурированные данные
* Имеет встроенную среду для работы с запросами



### Composer

* Google Composer
  * = Managed Apache Airflow
* Развертывается в виде кластера из нескольких машин с хранилищем в GCS



### Composer (Airflow)

* Apache Airflow — платформа для автоматического управления задачами, их расписанием и мониторингом
* Изначально был разработан в AirBNB
* Задачи описываются на Python
* Поддерживает сложные процессы из нескольких задач с зависимостями, ветвлением, условиями
* Удобный интерфейс
* Может масштабироваться с использованием Celery
* Легко расширяется под конкретные задачи



### Когда подходят облака

* Отсутствие DevOps-компетенций в команде
* Работа с зарубежными данными
* Гео-распределенные сервисы
* Автономные команды
* Дополнительные плюсы облака
  * Более точная оценка затрат
  * Ускорение циклов разработки
  * "Защита из коробки"
  * Интеграция продуктов



### Мотивация для перехода с собственной инфраструктуры на облако

* Избавление от нерелевантной деятельности
* Экономия на поддержке
* Миграция с Kafka на Pub/Sub
* Миграция со стека Spark + Hadoop на Dataflow
* Использование BigQuery



